import React, {Component} from 'react'
import actions from '../../redux/actions';
import {connect} from 'react-redux';
//import './UpdateForm.style.css';

class UpdateFrom extends Component<any, any> {

    public todoInput: any = React.createRef()

    constructor(props: any) {
        super(props);
        this.state = {
            input: props.todo.text
        }
    }

    editTodo = (event: any) => {
        event.preventDefault()
        if (!this.todoInput.current.value || /^\s*$/.test(this.todoInput.current.value)) {
            this.todoInput.current.value = ""
            return

        }
        debugger;
        this.props.stopEditing({text: this.todoInput.current.value, id: this.props.todo.id})
        this.todoInput.current.value = ""
        console.log()
    }

    onInput = (event: any) => {
        this.setState({input: event.target.value})
    }

    render() {
        return (
            <form onSubmit={this.editTodo}>
                <input className='todo-input edit'
                       placeholder='Add todo'
                       value={this.state.input}
                       ref={this.todoInput}
                       onInput={this.onInput}/>

                <button className='todo-button edit'
                        type='submit'>Update
                </button>
            </form>)
    }
}

const mapDispatchToProps = (dispatch: any) => ({
    stopEditing: (todo: any) => dispatch(actions.stopEditing(todo))
})

const mapStateToProps = (state: any) => ({
    todos: state.todos,
})


export default connect(mapStateToProps, mapDispatchToProps)(UpdateFrom)
