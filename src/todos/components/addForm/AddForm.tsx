import React, {Component} from 'react'
import {connect} from 'react-redux'
import actions from '../../redux/actions'
import "./AddForm.style.css"

class AddFrom extends Component<any, any> {

    public todoInput: any = React.createRef()

    addTodo = (event: any) => {
        event.preventDefault() //It has to be on  top to prevent refreshing page while input is empty
        if (!this.todoInput.current.value || /^\s*$/.test(this.todoInput.current.value)) {
            return
        }
        this.props.add({text: this.todoInput.current.value})
        this.todoInput.current.value = ""
       
    }

    render() {
        return (
            <form className="todo-form" onSubmit={this.addTodo}>
                <div className='input-container'>
                    <input placeholder="Add your ToDo" className='todo-input' ref={this.todoInput}/>
                    <button className='todo-button' type='submit'>Add ToDo</button>
                </div>
            </form>)
    }
}

const mapDispatchToProps = (dispatch: any) => ({
    add: (todo: any) => dispatch(actions.add(todo)),
    remove: (todo: any) => dispatch(actions.remove(todo)),
})

export default connect(null, mapDispatchToProps)(AddFrom)
