import React, {Component} from 'react'
import Fab from '@material-ui/core/Fab';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import DoneIcon from '@material-ui/icons/Done';
import {createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';
import {connect} from 'react-redux';
import './Buttons.style.css'

import actions from '../../../redux/actions';


class Buttons extends Component<any, any> {

    theme = createMuiTheme({
        palette: {
            secondary: {
                main: '#38AECC',
            }
        }
    });

    onRemove = () => {
        this.props.remove(this.props.todo);
    }

    onEdit = () => {
        this.props.startEditing(this.props.todo);
    }

    onComplete = () => {
        this.props.complete(this.props.todo);
    }

    render() {
        return (
            <div className='buttons-container'>
                <MuiThemeProvider theme={this.theme}>
                    <Fab className='fab-edit custom-icon' onClick={this.onEdit} size="small" color="secondary" aria-label="edit">
                        <EditIcon/>
                    </Fab>
                    <Fab className='fab-delete custom-icon' onClick={this.onRemove} size="small" color="secondary"
                         aria-label="edit">
                        <DeleteIcon/>
                    </Fab>
                    <Fab className='fab-done-icon custom-icon' onClick={this.onComplete} size="small" color="secondary"
                         aria-label="edit">
                        <DoneIcon/>
                    </Fab>
                </MuiThemeProvider>
            </div>
        )
    }
};

const mapStateToProps = (state: any) => ({})

const mapDispatchToProps = (dispatch: any) => ({
    remove: (todo: any) => dispatch(actions.remove(todo)),
    complete: (todo: any) => dispatch(actions.complete(todo)),
    startEditing: (todo: any) => dispatch(actions.startEditing(todo)),
    stopEditing: (todo: any) => dispatch(actions.stopEditing(todo)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Buttons)
