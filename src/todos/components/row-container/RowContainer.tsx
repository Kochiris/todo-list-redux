import React, {Component} from 'react'
import {connect} from 'react-redux';

import './RowContainer.style.css';
import UpdateFrom from "../updateFrom/UpdateFrom";
import Buttons from "./buttons/Buttons";

class RowContainer extends Component<any, any> {

    render() {
        return this.props.todos.list.map((todo: any, index: number) => {
            debugger;
            if (todo.isEditing) {
                return (<UpdateFrom todo={todo}/>)
            }
            if (todo.isCompleted) {
                return (<div className="todo-row full complete" key={index}>
                    <div className='todo-text'>
                        {todo.text}
                    </div>
                </div>)
            } else {
                return (<div className="todo-row full" key={index}>
                    <div className='todo-text'>
                        {todo.text}
                    </div>
                    <Buttons todo={todo}/>
                </div>)
            }
        });
    }
}


const mapStateToProps = (state: any) => ({
    todos: state.todos,
})

export default connect(mapStateToProps, null)(RowContainer)
