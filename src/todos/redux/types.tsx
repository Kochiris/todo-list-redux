const ADD = "ADD";
const REMOVE = "REMOVE";
const START_EDITING = "START_EDITING";
const STOP_EDITING = "STOP_EDITING";
const COMPLETE = "COMPLETE";

// eslint-disable-next-line
export default {
  ADD,
  REMOVE,
  START_EDITING,
  STOP_EDITING,
  COMPLETE,
};
