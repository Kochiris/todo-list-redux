import types from "./types";

const add = (item: any) => ({
    type: types.ADD,
    text: item.text,
})

const remove = (item: any) => ({
    type: types.REMOVE,
    id: item.id
})

const startEditing = (item: any) => ({
    type: types.START_EDITING,
    id: item.id,
    isEditing: true,
});

const stopEditing = (item: any) => ({
    type: types.STOP_EDITING,
    text: item.text,
    id: item.id,
    isEditing: false,
});

const complete = (item: any) => ({
    type: types.COMPLETE,
    id: item.id
})


// eslint-disable-next-line
export default {
    add,
    remove,
    startEditing,
    stopEditing,
    complete
}
