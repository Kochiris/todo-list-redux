import types from './types'
import todos from './../mocks/todos.json'

interface Todo {
    id: number;
    text: string;
    isCompleted: boolean;
    isEditing: boolean;
}

const TODOS = {
    list: todos,
    lastId: 3
}

function todosReducer(state = TODOS, action: any) {
    switch (action.type) {
        case types.ADD:
            return {
                ...state,
                list: [...state.list, {text: action.text, id: state.lastId}],
                lastId: state.lastId + 1

            }
        case types.REMOVE:
            return ({
                ...state,
                list: [...state.list].filter((todo: Todo) => todo.id !== action.id),

            })
        case types.START_EDITING:
            return {
                ...state,
                list: state.list.map(todo => {
                    if (todo.id === action.id) {
                        todo.isEditing = true
                    }
                    return todo;
                })
            }
        case types.STOP_EDITING:
            debugger;
            return {
                ...state,
                list: state.list.map(todo => {
                    if (todo.id === action.id) {
                        todo.isEditing = false
                        todo.text = action.text
                    }
                    return todo;
                })
            }
        case types.COMPLETE:
            return {
                ...state,
                list: state.list.map(todo => {
                    if (todo.id === action.id) {
                        todo.isCompleted = true
                    }
                    return todo;
                })
            }
        default:
            return state
    }
}

export default todosReducer
