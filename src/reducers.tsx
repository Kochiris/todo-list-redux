import {combineReducers} from "redux";
import todosReducer from "./todos/redux";

const rootReducer = combineReducers({
    todos: todosReducer,
});

export default rootReducer
