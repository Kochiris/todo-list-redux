import React from 'react';
import './App.css';
import AddFrom from "./todos/components/addForm/AddForm";
import Container from "./todos/components/row-container/RowContainer";

function App() {
    return (
        <div className="todo-app">
            <h1>Todo List</h1>
            <AddFrom></AddFrom>
            <Container></Container>
        </div>
    );
}

export default App;
